import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { I18nServiceService } from '../i18n-service/i18n-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  language:string="hello";
  data= JSON.parse(sessionStorage.getItem("changlang"));


  constructor(translate: TranslateService, 
    private i18nService: I18nServiceService
    ) {
      translate.setDefaultLang(this.data);
      translate.use(this.data);
  }

  ngOnInit(): void { 
  //  this.i18nService.localeEvent.subscribe(locale => this.translate.use(locale));  
    this.i18nService.localeEvent.subscribe((locale) => {
      this.language=locale,
      console.log("my locale="+locale),
      console.log("data="+this.language), 
      sessionStorage.setItem('changlang',JSON.stringify(this.language))});  
   
  }
}
