import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { I18nServiceService } from './i18n-service/i18n-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngx-i18n';
  language:string="";
  data= JSON.parse(sessionStorage.getItem("changlang"));

  constructor( private translate: TranslateService,
    private i18nService: I18nServiceService
  ) {
     this.translate.setDefaultLang(this.data);
     this.translate.use(this.data);
  }
  NgOnInit(){
    
    console.log(this.data);
     
  }

  changeLocale(locale: string) {
    this.i18nService.changeLocale(locale); 
    this.language=locale;
    sessionStorage.setItem('changlang',JSON.stringify(this.language));
  }
}
